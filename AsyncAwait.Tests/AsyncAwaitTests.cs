namespace AsyncAwait.Tests
{
    using System;
    using System.Threading.Tasks;
    using NUnit.Framework;

    /// <summary>
    /// Test class.
    /// </summary>
    [TestFixture]
    public class AsyncAwaitTests
    {
        private const int MultiplicationFactor = 10;
        private static readonly int[] EmptyArray = Array.Empty<int>();
        private static readonly int[] PreFilledArray = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void MultipliesArrayByRandomNumber_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.MultipliesArrayByRandomNumberAsync(EmptyArray, MultiplicationFactor));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void MultipliesArrayByRandomNumber_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.MultipliesArrayByRandomNumberAsync(null, MultiplicationFactor));
        }

        /// <summary>
        /// Testing MultipliesArrayByRandomNumber method.
        /// </summary>
        /// <returns>Task.</returns>
        [Test]
        public async Task MultipliesArrayByRandomNumber_Returns_Array()
        {
            var expected = new int[] { 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };

            var act = await AsyncAwait.MultipliesArrayByRandomNumberAsync(PreFilledArray, MultiplicationFactor);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.SortArrayByAscendingAsync(EmptyArray));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void SortArrayByAscending_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.SortArrayByAscendingAsync(null));
        }

        /// <summary>
        /// Testing SortArrayByAscending method.
        /// </summary>
        /// <returns>Task.</returns>
        [Test]
        public async Task SortArrayByAscending_Returns_Array()
        {
            var expected = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

            var act = await AsyncAwait.SortArrayByAscendingAsync(PreFilledArray);

            Assert.That(act, Is.EqualTo(expected));
        }

        /// <summary>
        /// Test if passing empty array throws an exception.
        /// </summary>
        [Test]
        public void AverageOfTheArrayElements_Throws_ArgumentException_When_Empty_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentException>(async () => await AsyncAwait.AverageOfTheArrayElementsAsync(EmptyArray));
        }

        /// <summary>
        /// Testing if passing null throws an exception.
        /// </summary>
        [Test]
        public void AverageOfTheArrayElements_Throws_ArgumentException_When_Null_Array_Is_passed()
        {
            Assert.ThrowsAsync<ArgumentNullException>(async () => await AsyncAwait.AverageOfTheArrayElementsAsync(null));
        }

        /// <summary>
        /// Testing AverageOfTheArrayElements method.
        /// </summary>
        /// <returns>Task.</returns>
        [Test]
        public async Task AverageOfTheArrayElements_Returns_Array()
        {
            var expected = 5.5;

            var act = await AsyncAwait.AverageOfTheArrayElementsAsync(PreFilledArray);

            Assert.That(act, Is.EqualTo(expected));
        }
    }
}