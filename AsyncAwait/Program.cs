﻿namespace AsyncAwait
{
    /// <summary>
    /// Class programm.
    /// </summary>
    internal static class Program
    {
        /// <summary>
        /// Entry point.
        /// </summary>
        /// <returns><<see cref="Task"/>Representing the asynchronous operation.</returns>
        internal static async Task Main()
        {
            try
            {
                var result = await AsyncAwait.RunAsync(10, 1, 10);
                Console.WriteLine($"\nresult: {result}");
            }
            catch (AggregateException ex)
            {
                foreach (var e in ex.Flatten().InnerExceptions)
                {
                    Console.WriteLine($"\n{e.GetType().Name}\t{e.Message}");
                }
            }
        }
    }
}
