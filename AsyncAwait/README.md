﻿The program should chain four tasks:

1) creates an array of 10 random integers.
2) multiplies the array by a randomly generated number.
3) sorts the array by ascending.
4) calculates the average value.