﻿namespace AsyncAwait
{
    /// <summary>
    /// Class for async await.
    /// </summary>
    public static class AsyncAwait
    {
        private static readonly Random Random = new ();
        private static readonly object DisplayLock = new ();

        /// <summary>
        /// Multiply array elements by random number.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <param name="number">Random number.</param>
        /// <returns>Integer array.</returns>
        public static async Task<int[]> MultipliesArrayByRandomNumberAsync(int[] arr, int number)
        {
            return await Task.Run(() =>
            {
                ExceptionHelper.ArrayIsNullOrEmpty(arr);

                var result = arr.Select(x => x * number).ToArray();
                return result;
            });
        }

        /// <summary>
        /// Sort array in ascending order.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <returns>Sorted array.</returns>
        public static async Task<int[]> SortArrayByAscendingAsync(int[] arr)
        {
            return await Task.Run(() =>
            {
                ExceptionHelper.ArrayIsNullOrEmpty(arr);

                var result = arr.OrderBy(x => x).ToArray();
                return result;
            });
        }

        /// <summary>
        /// Take average of array elements.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <returns>Average.</returns>
        public static async Task<double> AverageOfTheArrayElementsAsync(int[] arr)
        {
            return await Task.Run(() =>
            {
                ExceptionHelper.ArrayIsNullOrEmpty(arr);

                var result = arr.Average();
                return result;
            });
        }

        /// <summary>
        /// Chain tasks.
        /// </summary>
        /// <param name="arrayLenght">Array length.</param>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <returns>Average result.</returns>
        public static async Task<double> RunAsync(int arrayLenght, int from, int to)
        {
            List<Task> tasks = new () { };
            ExceptionHelper.ValueLessThanDefined(arrayLenght, 1);
            ExceptionHelper.ValueLessThanDefined(to, from);

            var taskCreateArray = CreateArrayOfRandomIntegersAsync(from, to, arrayLenght);
            var taskGenerateRandomNumber = GenerateRandomIntegerAsync(from, to);

            await Task.WhenAll(taskCreateArray, taskGenerateRandomNumber);
            var arrayCreated = await taskCreateArray;
            var randomNumber = await taskGenerateRandomNumber;

            var taskMultiplyArrayByNumber = MultipliesArrayByRandomNumberAsync(arrayCreated, randomNumber);
            tasks.Add(ShowMessageAsync($"Random number {randomNumber} generated."));
            tasks.Add(PrintAsync(arrayCreated, "Created array."));

            var multipliedArrayByRandomNumber = await taskMultiplyArrayByNumber;
            var taskCalculateAverage = AverageOfTheArrayElementsAsync(multipliedArrayByRandomNumber);
            var taskSortArray = SortArrayByAscendingAsync(multipliedArrayByRandomNumber);
            tasks.Add(PrintAsync(multipliedArrayByRandomNumber, "Multiplied array by random number"));

            List<Task> finalTasks = new () { taskSortArray, taskCalculateAverage };
            while (finalTasks.Count > 0)
            {
                var finishedTask = await Task.WhenAny(finalTasks);
                if (finishedTask == taskSortArray)
                {
                    var sortedArray = await taskSortArray;
                    tasks.Add(PrintAsync(sortedArray, "Ordered array."));
                }

                if (finishedTask == taskCalculateAverage)
                {
                    var calculatedAverage = await taskCalculateAverage;
                    tasks.Add(ShowMessageAsync($"Average value {calculatedAverage}"));
                }

                finalTasks.Remove(finishedTask);
            }

            await Task.WhenAll(tasks);

            return taskCalculateAverage.Result;
        }

        /// <summary>
        /// Create array of random integers.
        /// </summary>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <param name="arrayLength">Array length.</param>
        /// <returns>Array.</returns>
        private static async Task<int[]> CreateArrayOfRandomIntegersAsync(int from, int to, int arrayLength)
        {
            ExceptionHelper.ValueLessThanDefined(arrayLength, 1);
            ExceptionHelper.ValueLessThanDefined(to, from);

            var arr = new int[arrayLength];
            for (int i = 0; i < arrayLength; i++)
            {
                arr[i] = await GenerateRandomIntegerAsync(from, to);
            }

            return arr;
        }

        /// <summary>
        /// Generate random integers.
        /// </summary>
        /// <param name="from">Array element lower bound.</param>
        /// <param name="to">Array element upper bound.</param>
        /// <returns>Random integer.</returns>
        private static async Task<int> GenerateRandomIntegerAsync(int from, int to)
        {
            return await Task.Run(() =>
            {
                ExceptionHelper.ValueLessThanDefined(to, from);

                lock (Random)
                {
                    return Random.Next(from, to + 1);
                }
            });
        }

        /// <summary>
        /// Print array.
        /// </summary>
        /// <param name="arr">Array.</param>
        /// <param name="title">Title.</param>
        private static async Task PrintAsync(int[] arr, string title)
        {
            await Task.Run(() =>
            {
                ExceptionHelper.StringNullOrEmpty(title);
                ExceptionHelper.ArrayIsNullOrEmpty(arr);

                lock (DisplayLock)
                {
                    Console.WriteLine($"\n{title}");
                    for (int i = 0; i < arr.Length; i++)
                    {
                        Console.WriteLine($"\t{arr[i]}");
                    }
                }
            });
        }

        /// <summary>
        /// Show message.
        /// </summary>
        /// <param name="message">Message.</param>
        private static async Task ShowMessageAsync(string message)
        {
            await Task.Run(() =>
            {
                ExceptionHelper.StringNullOrEmpty(message);

                lock (DisplayLock)
                {
                    Console.WriteLine($"\n{message}");
                }
            });
        }
    }
}
