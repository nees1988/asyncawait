﻿namespace AsyncAwait
{
    /// <summary>
    /// Exception helper class.
    /// </summary>
    public static class ExceptionHelper
    {
        /// <summary>
        /// Check if string is null or empty.
        /// </summary>
        /// <param name="str">String parameter.</param>
        /// <exception cref="ArgumentNullException">Argument null exception for string.</exception>
        /// <exception cref="ArgumentException">Argument exception for string.</exception>
        public static void StringNullOrEmpty(string str)
        {
            if (str == null)
            {
                throw new ArgumentNullException(nameof(str), "The string is null");
            }

            if (string.IsNullOrWhiteSpace(str))
            {
                throw new ArgumentException($"Given string is empty", nameof(str));
            }
        }

        /// <summary>
        /// Check if array is null or empty.
        /// </summary>
        /// <param name="array">Array.</param>
        /// <exception cref="ArgumentNullException">Argument null exception for array.</exception>
        /// <exception cref="ArgumentException">Argument exception for array.</exception>
        public static void ArrayIsNullOrEmpty(int[] array)
        {
            if (array == null)
            {
                throw new ArgumentNullException(nameof(array), "The array is null");
            }

            if (array.Length == 0)
            {
                throw new ArgumentException($"The array is empty", nameof(array));
            }
        }

        /// <summary>
        /// Check if higher value is less than minimum value.
        /// </summary>
        /// <param name="value">Value.</param>
        /// <param name="minimumValue">Minimum value.</param>
        /// <exception cref="ArgumentException">Argument exception for value.</exception>
        public static void ValueLessThanDefined(int value, int minimumValue)
        {
            if (value < minimumValue)
            {
                throw new ArgumentException($"The value cannot be less than minimum value", nameof(value));
            }
        }
    }
}
